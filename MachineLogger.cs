﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using ServiceContracts.GatewayServiceContracts;

namespace SQLDataLogger
{
    public class MachineLogger
    {
        string _connectionString;
        MachineFileConfig myConfig;
        DataTable _logDT;
        SQLDALogging da;
        public MachineLogger(string connectionString, MachineFileConfig config)
        {
            _connectionString = connectionString;
            myConfig = config;
            da = new SQLDALogging(_connectionString, config.MachineName, config.Prj, config.Part, config.Inst, config.RecordID);
            _logDT = da.getDT();
        }

        public void LogEvent(string eventType, string eventDetails)
        {
            DataRow row = _logDT.NewRow();
            row["EventName"] = eventType;
            row["EventDetail"] = eventDetails;
            _logDT.Rows.Add(row);
            UploadLog();
        }

        private void UploadLog()
        {
            da.updateDT(_logDT);
        }
    }
}
