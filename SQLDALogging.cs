﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace SQLDataLogger
{
    class SQLDALogging
    {
        public SQLDALogging(string connectionString, string machineName, string prj, string part, string inst, string IDNum)
        {
            _conn = new SqlConnection(connectionString);
            _machineName = machineName;
            _prj = prj;
            _part = part;
            _inst = inst;
            _IDNum = IDNum;
        }
        SqlConnection _conn;
        string _machineName;
        string _prj;
        string _part;
        string _inst;
        string _IDNum;
        SqlConnection Conn
        {
            get { return _conn; }
            set { _conn = value; }
        }

        private SqlDataAdapter Adapter()
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM MachineLog WHERE MachineName = @machineName", _conn);
            cmd.Parameters.AddWithValue("@machineName", _machineName);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.MissingSchemaAction = MissingSchemaAction.AddWithKey;
            SqlCommandBuilder bld = new SqlCommandBuilder(da);
            return da;
        }

        public DataTable getDT()
        {
            DataTable dt = new DataTable("MachineLog");
            SqlDataAdapter da = this.Adapter();
            da.Fill(dt);
            dt.TableNewRow += dt_TableNewRow;
            return dt;
        }

        void dt_TableNewRow(object sender, DataTableNewRowEventArgs e)
        {
            e.Row["EventIDNum"] = Guid.NewGuid();
            e.Row["MachineName"] = _machineName;
            e.Row["prj"] = _prj;
            e.Row["part"] = _part;
            e.Row["inst"] = _inst;
            if (_IDNum == null)
                e.Row["IDNum"] = DBNull.Value;
            else
                e.Row["IDNum"] = _IDNum;
            e.Row["TimeStamp"] = DateTime.Now;
        }

        public DataTable getSchema()
        {
            SqlCommand cmd = new SqlCommand("SELECT TOP 0 * FROM MachineLog", _conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.MissingSchemaAction = MissingSchemaAction.AddWithKey;
            SqlCommandBuilder cmbBld = new SqlCommandBuilder(da);
            DataTable dt = new DataTable("MachineLog");
            da.Fill(dt);
            dt.TableNewRow += dt_TableNewRow;
            return dt;
        }

        public void updateDT(DataTable dt)
        {
            try
            {
                SqlDataAdapter da = this.Adapter();
                da.Update(dt);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
