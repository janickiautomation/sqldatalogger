﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLDataLogger
{
    public static class MachineEventTypes
    {
        public static string LoadEvent = "LoadEvent";
        public static string UnloadEvent = "UnloadEvent";
        public static string FileRun = "FileRun";
        public static string FileStop = "FileStop";
        public static string ResetPressed = "ResetPressed";
        public static string InProcessToolChange = "InProcessToolChangeCalled";
        public static string ToolRemoved = "ToolRemoved";
        public static string ToolLoaded = "ToolLoaded";
        public static string FatalError = "FatalError";
        public static string GatewayStarted = "GatewayStarted";
        public static string DeploymentTest = "GatewayDeploymentTest";
        public static string LostConnection = "Lost840DConnection";
        public static string ToolError = "ToolError";

    }
}
